import hashlib
import os
import sys
from os import listdir
import argparse
import json
from config import *


def CalcMD5(filepath):
    with open(filepath,'rb') as f:
        md5obj = hashlib.md5()
        md5obj.update(f.read())
        hash = md5obj.hexdigest()
        #print(hash)
        return hash
def convent_meta(filepath):
    mome_use  = []
    mome_hash = []
    pc_use    = []
    pc_hash   = []
    if filepath == 'basket_ball_kok':
        cfg       = basket_ball_kok_config.base_model_config()
    if filepath == 'hello_kitty_car':
        cfg       = hello_kitty_car_config.base_model_config()
    if filepath == 'hello_kitty_home':
        cfg       = hello_kitty_home_config.base_model_config()
    file_list = listdir('./'+str(filepath))
    for files in file_list:
    #file_hash.append(CalcMD5('./basket_ball_kok/'+str(files)))
        if (files.split('.')[1] =='json'):
            pass
        elif ( files.split('.')[1] == 'anchors' )|( files.split('.')[1] == 'bin' )|( files.split('.')[1] =='param' ):
            mome_use.append(files)
            mome_hash.append(CalcMD5('./'+str(filepath)+'/'+str(files)))
        else:
            pc_use.append(files)
            pc_hash.append(CalcMD5('./'+str(filepath)+'/'+str(files)))
    return cfg ,mome_use,mome_hash,pc_use,pc_hash


#cfg = base_model_config()
def parse_arguments():
    parser = argparse.ArgumentParser(description='face recognition aPI tester')

    parser.add_argument('--file_list', default=None, type=str,
                        help='input modle ex:hello_kitty_home or hello_kitty_car or basket_ball_kok')
    parser.add_argument('--confidence', default=0.5, type=str,
                        help='confidence')


    args = parser.parse_args()

    return args
if __name__ == "__main__":
    arguments = parse_arguments()
    
    if ( arguments.file_list == None):
        print('no_input')
        file_list = listdir('./config')
        for files in file_list:
            if (files.split('_')[-1] == 'config.py'):
                arguments.file_list = (files.split('_config.py')[0])
                print(arguments.file_list)
                if ( arguments.file_list == 'basket_ball_kok'):
                    arguments.confidence = 0.3
                    display = [ 'basket' , 'basketball' , 'score' ]
                else:
                    arguments.confidence = 0.5
                    if ( arguments.file_list == 'hello_kitty_home' ):
                        display = [ 'handshake' , 'thumb-up' , 'hand-five' , 'has_person' ]
                    elif ( arguments.file_list == 'hello_kitty_car' ) :
                        display = [ 'car' ]
                    else:
                        display = []
                cfg ,mome_use , mome_hash , pc_use , pc_hash = convent_meta(arguments.file_list)
                response = {
                    "name": arguments.file_list,
                    "url": 'http://mome.oss-cn-shanghai.aliyuncs.com/model/'+str(arguments.file_list)+'/',
                    "file_pc": {
                        "file" : pc_use,
                        "hash": pc_hash
                    },
                    "file_mome": {
                        "file" : mome_use,
                        "hash": mome_hash
                    },
                    "types": cfg.CLASS_NAMES,
                    "types_display":display,
                    "width": cfg.IMAGE_WIDTH,
                    "height": cfg.IMAGE_HEIGHT,
                    "confidence": arguments.confidence
                }
                print(response)
                with open('./'+arguments.file_list+'/meta.json', 'w') as dumpfile:
                    json.dump(response, dumpfile)

    else:
        cfg ,mome_use , mome_hash , pc_use , pc_hash = convent_meta(arguments.file_list)
        print(cfg.CLASS_NAMES)
        if ( arguments.file_list == 'basket_ball_kok'):
            display = [ 'basket' , 'basketball' , 'score' ]
        elif ( arguments.file_list == 'hello_kitty_home' ):
            display = [ 'handshake' , 'thumb-up' , 'hand-five' , 'has_person' ]
        elif ( arguments.file_list == 'hello_kitty_car' ) :
            display = [ 'car' ]
        else:
            display = []
        #print(file_list)
        response = {
            "name": arguments.file_list,
            "url": 'http://mome.oss-cn-shanghai.aliyuncs.com/model/'+str(arguments.file_list)+'/',
            "file_pc": {
                "file" : pc_use,
                "hash": pc_hash
            },
            "file_mome": {
                "file" : mome_use,
                "hash": mome_hash
            },
            "types": cfg.CLASS_NAMES,
            "types_display":display,
            "width": cfg.IMAGE_WIDTH,
            "height": cfg.IMAGE_HEIGHT,
            "confidence": arguments.confidence
        }
        print(response)
        with open('./'+arguments.file_list+'/meta.json', 'w') as dumpfile:
            json.dump(response, dumpfile)
